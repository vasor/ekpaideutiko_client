import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { MathService } from '../../services/mathService';
import { AuthService } from '../../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmComponent } from '../../confirm.component';
import { DialogService } from "ng2-bootstrap-modal";
@Component({
    selector: 'test',
    templateUrl: './test.html',
    styleUrls: ['./test.css']
})
export class TestPage implements OnInit, OnDestroy {
    // public user: any;
    private sub: any;
    private category: any;
    private message: string;
    private clicked: Boolean;
    private easy: Boolean;
    private difficult: Boolean;
    private noresults: Boolean;
    private exercises: any;
    private counter: number;
    private answer: string;
    private answeredButton: Boolean;
    private answers: any;
    private finished: Boolean;
    private user_id: any;
    private total: number;
    private rights: number;
    private diagnosis: string;
    constructor(private dialogService: DialogService, public userService: User, public auth: AuthService, public router: Router, public route: ActivatedRoute, private mathService: MathService) {
        this.noresults = false;
        this.message = "";
        this.easy = false;
        this.clicked = false;
        this.difficult = false;
        this.exercises = [];
        this.counter = 0;
        this.answer = "";
        this.answeredButton = false;
        this.answers = [];
        this.finished = false;
        this.user_id = this.auth.user;
        this.total = 0;
        this.rights = 0;
        this.diagnosis = "";

    }

    ngOnInit() {
        this.user_id = this.auth.user;
        console.log(this.user_id);
        this.sub = this.route.params.subscribe(params => {
            console.log(params);
            this.category = params['category']; // (+) converts string 'id' to a number
            console.log(this.category);
            // In a real app: dispatch action to load the details here.
        });

        switch (this.category) {
            case "addition":
                this.mathService.loadTests("πρόσθεση", this.auth.user).then((data) => {
                    this.exercises = data;
                    let c = 0;
                    this.exercises.forEach(element => {
                        element['index'] = c;
                        c++;
                    });
                    console.log(this.exercises);
                    this.total = this.exercises.length;
                }, (error) => {

                    console.log(error);
                });
                break;
            case "substraction":
                this.mathService.loadTests("αφαίρεση", this.auth.user).then((data) => {
                    this.exercises = data;
                    let c = 0;
                    this.exercises.forEach(element => {
                        element['index'] = c;
                        c++;
                    });
                    console.log(this.exercises);
                    this.total = this.exercises.length;
                }, (error) => {

                    console.log(error);
                });
                break;
            case "division":
                this.mathService.loadTests("διαίρεση", this.auth.user).then((data) => {
                    this.exercises = data;
                    let c = 0;
                    this.exercises.forEach(element => {
                        element['index'] = c;
                        c++;
                    });
                    console.log(this.exercises);
                    this.total = this.exercises.length;
                }, (error) => {

                    console.log(error);
                });
                break;
            case "multiplication":
                this.mathService.loadTests("πολλαπλασιασμός", this.auth.user).then((data) => {
                    this.exercises = data;
                    let c = 0;
                    this.exercises.forEach(element => {
                        element['index'] = c;
                        c++;
                    });
                    console.log(this.exercises);
                    this.total = this.exercises.length;
                }, (error) => {

                    console.log(error);
                });
                break;
            case "general":
                this.mathService.loadGeneralTests().then((data) => {
                    this.exercises = data;
                    let c = 0;
                    this.exercises.forEach(element => {
                        element['index'] = c;
                        c++;
                    });
                    console.log(this.exercises);
                    this.total = this.exercises.length;
                }, (error) => {

                    console.log(error);
                });
                break;
            default:
                this.noresults = true;
                break;
        }


    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    easyClicked(easiness) {
        this.clicked = true;
        if (easiness == 'easy') {
            this.easy = true;
        }
        else {
            this.difficult = true;
        }
    }
    //eukola
    answered(answer) {

        let success: string;
        this.answeredButton = true;
        if (answer.result == this.answer) {
            success = "ΣΩΣΤΑ";
            this.rights++;
            this.message = "ΣΥΓΧΑΡΗΤΗΡΙΑ!"
        } else {
            success = "ΛΑΘΟΣ";
            // DIAGNOSIS
            // Αν είναι αποτέλεσμα κάποιας άλλης πράξης, τότε η διάγνωση είναι ότι έπρεπε να είναι απροσεξία
            let ans = parseInt(this.answer)
            if (ans == (answer.op1 - answer.op2) || ans == (answer.op1 * answer.op2) || ans == (answer.op1 / answer.op2) || ans == (answer.op1 + answer.op2)) {
                this.message = "Πρέπει να είσαι πιο προσεκτικός/ή, καθώς η πράξη είναι " + answer.category;
                this.diagnosis = "απροσεξία"
            }
            else {


                // Αν κάποιο ψηφίο διαφέρει κατά μία μονάδα, δηλαδη πιθανως να μην υπολόγισε το κρατούμενο
                // , τότε η διάγνωση είναι απροσεξία
                let count_one = 0;
                let count_many = 0;

                if (answer.result.toString().length != this.answer.toString().length) {
                    if (this.answer.toString().length > answer.result.toString().length) {
                        let diff = this.answer.toString().length - answer.result.toString().length;
                        for (let c = 0; c < diff; c++) {
                            answer.result = "0" + answer.result;
                            console.log(answer.result)
                        }

                    }
                    else {
                        let diff = answer.result.toString().length - this.answer.toString().length;
                        for (let c = 0; c < diff; c++) {
                            this.answer = "0" + this.answer;
                            console.log(this.answer)
                        }

                    }
                }
                // console.log("answer.result.length ", answer.result.toString().length)
                for (let i = answer.result.toString().length - 1; i >= 0; i--) { // εξεταζουμε απο το τελευταιο ψηφιο προς το πρωτο
                    console.log("ΓΡΑΜΜΑ", i, "ΨΗΦΙΑ", parseInt(this.answer.toString()[i]), parseInt(answer.result.toString()[i]))

                    if (this.answer.toString()[i]) { //αν υπάρχει ψηφίο

                        if ((parseInt(this.answer.toString()[i]) - parseInt(answer.result.toString()[i])) == 1) { //an h diafora einai mias monadas
                            count_one++;
                            console.log("DIAFORA EINAI ENA")
                        }
                        else if ((parseInt(this.answer.toString()[i]) - parseInt(answer.result.toString()[i])) > 1) { // an h diafora einai megalyterh tou enos
                            count_many++;
                            console.log("DIAFORA EINAI MEGALYTERH TOU ENOS")

                        }

                    }
                }
                if (count_many > 0) {
                    this.message = "Θα πρέπει να ξαναδιαβάσεις το κεφάλαιο " + answer.category;
                    this.diagnosis = "αδιάβαστος/η"
                }
                else {
                    this.message = "Θα πρέπει να είσαι πιο προσεκτικός/ή";
                    this.diagnosis = "απροσεξία"
                }

            }


        }
        this.mathService.saveAnswer(answer._id, answer.category, this.auth.user, success, this.diagnosis).subscribe((data) => {
            console.log(data)
        }, (error) => {
            console.log(error)
        });
        this.answers[this.answers.length] = answer;
        this.answers[this.answers.length - 1].success = success;
        this.answers[this.answers.length - 1].ans = this.answer;

        this.answers[this.answers.length - 1].diagnosis = this.diagnosis;
        this.answers[this.answers.length - 1].message = this.message;

        console.log(this.answers)

    }
    //dyskola
    answeredB(answer) {
        let ans = this.answer;
        let success: String;
        this.answeredButton = true;
        if (answer.category == 'πρόσθεση') {
            if (answer.op3 == this.answer) {
                success = "ΣΩΣΤΑ";
                this.rights++;
                this.message = "ΣΥΓΧΑΡΗΤΗΡΙΑ!"
            } else {
                success = "ΛΑΘΟΣ";
                //DIAGNOSIS

                let count_one = 0;
                let count_many = 0;
                if (answer.op3.toString().length != this.answer.toString().length) {
                    if (this.answer.toString().length > answer.op3.toString().length) {
                        let diff = this.answer.toString().length - answer.result.toString().length;
                        for (let c = 0; c < diff; c++) {
                            answer.op3 = "0" + answer.op3;
                            console.log(answer.op3)
                        }

                    }
                    else {
                        let diff = answer.op3.toString().length - this.answer.toString().length;
                        for (let c = 0; c < diff; c++) {
                            this.answer = "0" + this.answer;
                            console.log(this.answer)
                        }
                    }
                }



                for (let i = answer.op3.toString().length - 1; i >= 0; i--) { // εξεταζουμε απο το τελευταιο ψηφιο προς το πρωτο
                    // console.log("ΓΡΑΜΜΑ", i, "ΨΗΦΙΑ", parseInt(this.answer.toString()[i]), parseInt(answer.op3.toString()[i]))
                    if (this.answer.toString()[i] && answer.op3.toString()[i]) { //αν υπάρχει ψηφίο

                        if ((parseInt(this.answer.toString()[i]) - parseInt(answer.op3.toString()[i])) == 1) { //an h diafora einai mias monadas
                            count_one++;
                            // console.log("DIAFORA EINAI ENA")
                        }
                        else if ((parseInt(this.answer.toString()[i]) - parseInt(answer.op3.toString()[i])) > 1) { // an h diafora einai megalyterh tou enos
                            count_many++;
                            // console.log("DIAFORA EINAI MEGALYTERH TOU ENOS")

                        }

                    }
                }
                if (count_many > 0) {
                    this.message = "Θα πρέπει να ξαναδιαβάσεις το κεφάλαιο με την " + answer.category;
                    this.diagnosis = "αδιάβαστος/η"
                }
                else {
                    this.message = "Θα πρέπει να είσαι πιο προσεκτικός/ή";
                    this.diagnosis = "απροσεξία"
                }

            }
        }
        else {
            if (answer.op2 == this.answer) {
                success = "ΣΩΣΤΑ";
                this.rights++;
                this.message = "ΣΥΓΧΑΡΗΤΗΡΙΑ!"
            } else {
                success = "ΛΑΘΟΣ";
                //DIAGNOSIS

                let count_one = 0;
                let count_many = 0;
                if (answer.op2.toString().length != this.answer.toString().length) {
                    if (this.answer.toString().length > answer.op2.toString().length) {
                        let diff = this.answer.toString().length - answer.result.toString().length;
                        for (let c = 0; c < diff; c++) {
                            answer.op2 = "0" + answer.op2;
                            console.log(answer.op2)
                        }

                    }
                    else {
                        let diff = answer.op2.toString().length - this.answer.toString().length;
                        for (let c = 0; c < diff; c++) {
                            this.answer = "0" + this.answer;
                            console.log(this.answer)
                        }
                    }
                }
                for (let i = answer.op2.toString().length - 1; i >= 0; i--) { // εξεταζουμε απο το τελευταιο ψηφιο προς το πρωτο
                    console.log("ΓΡΑΜΜΑ", i, "ΨΗΦΙΑ", parseInt(this.answer.toString()[i]), parseInt(answer.op2.toString()[i]))
                    if (this.answer.toString()[i] && answer.op2.toString()[i]) { //αν υπάρχει ψηφίο

                        if ((parseInt(this.answer.toString()[i]) - parseInt(answer.op2.toString()[i])) == 1) { //an h diafora einai mias monadas
                            count_one++;
                            console.log("DIAFORA EINAI ENA")
                        }
                        else if ((parseInt(this.answer.toString()[i]) - parseInt(answer.op2.toString()[i])) > 1) { // an h diafora einai megalyterh tou enos
                            count_many++;
                            console.log("DIAFORA EINAI MEGALYTERH TOU ENOS")

                        }

                    }
                }
                if (count_many > 0 || count_one > 1) {
                    this.message = "Θα πρέπει να ξαναδιαβάσεις το κεφάλαιο με την " + answer.category;
                    this.diagnosis = "αδιάβαστος/η"
                }
                else {
                    this.message = "Θα πρέπει να είσαι πιο προσεκτικός/ή";
                    this.diagnosis = "απροσεξία"
                }
            }
        }

        this.mathService.saveAnswer(answer._id, answer.category, this.auth.user, success, this.diagnosis).subscribe((data) => {
            console.log(data)
        }, (error) => {
            console.log(error)
        });
        this.answers[this.answers.length] = answer;
        this.answers[this.answers.length - 1].success = success;
        this.answers[this.answers.length - 1].ans = this.answer;
        this.answers[this.answers.length - 1].diagnosis = this.diagnosis;
        this.answers[this.answers.length - 1].message = this.message;
        console.log(this.answers)

    }

    next(index) {
        this.counter = index + 1;
        this.answer = "";
        this.message = "";
        this.answeredButton = false;
        // console.log(index, this.exercises.length)
        if (index + 1 == this.exercises.length) {
            this.finished = true;

        }
    }

    deleteExercise(ex_id) {
        let disposable = this.dialogService.addDialog(ConfirmComponent, {
            title: 'ΕΠΙΒΕΒΑΙΩΣΗ',
            message: 'Διαγραφή?'
        })
            .subscribe((isConfirmed) => {
                //We get dialog result
                if (isConfirmed) {
                    // alert('accepted');
                    console.log(ex_id)
                    this.mathService.deleteExercise(ex_id).subscribe((data) => {
                        console.log("success");
                        location.reload();
                    }, (error) => {
                        console.log("not successful");
                        this.router.navigateByUrl('/theory')
                    }), () => {
                        console.log("function completed")
                    };
                }
                else {
                    // alert('declined');
                }
            });

        setTimeout(() => {
            disposable.unsubscribe();
        }, 10000);

    }




}

