import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { MathService } from '../../services/mathService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'add-content',
    templateUrl: './add-content.html',
    styleUrls: ['./add-content.css']
})
export class AddContentPage {
    private title: String;
    private description: String;
    private category: String;
    private sub: any;
    constructor(private mathService: MathService, private route: ActivatedRoute, private router: Router) {
        this.mathService = mathService;


    }

    ngOnInit() {
        this.route.queryParams.subscribe((params) => {
            this.category = params['category'];
        });
    }

    addContent() {
        let item = { 'title': this.title, 'description': this.description, 'category': this.category }
        this.mathService.addContent(item).subscribe((data) => {
            console.log(data);
            console.log(this.category);
            switch (this.category) {
                case 'πρόσθεση': {
                    this.router.navigateByUrl('/theory/addition');
                    break;
                }
                case 'αφαίρεση': {
                    this.router.navigateByUrl('/theory/substraction');
                    break;
                }
                case 'διαίρεση': {
                    this.router.navigateByUrl('/theory/division');
                    break;
                }
                case 'πολλαπλασιασμός': {
                    this.router.navigateByUrl('/theory/multiplication');
                    break;
                }

            }
        }, (error) => {
            console.log(error);
        }, () => {

        });
    }

}
