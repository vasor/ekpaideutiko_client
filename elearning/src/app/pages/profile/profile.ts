import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { MathService } from '../../services/mathService';
import { AuthService } from '../../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    selector: 'profile',
    templateUrl: './profile.html',
    styleUrls: ['./profile.css']
})
export class ProfilePage implements OnInit, OnDestroy {
    public user: any;
    private sub: any;
    private username: any;
    private message: String;
    private noresult: Boolean;
    public stats: any;
    public error: Boolean;
    public diagnosis: any;
    public count: number;
    constructor(public userService: User, public auth: AuthService, public router: Router, public route: ActivatedRoute, private mathService: MathService) {
        this.user = {};
        this.noresult = false;
        this.message = "";
        this.stats = [];
        this.diagnosis = "";
        this.count = 0;

    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            console.log(params);
            this.username = params['username']; // (+) converts string 'id' to a number
            console.log(this.username);
            // In a real app: dispatch action to load the details here.
        });
        this.mathService.getProfileInfo(this.username).subscribe((data) => {
            console.log(data);
            this.user = data;
            this.userService.getStatistics(this.user.id).then((data) => {
                this.stats = data;
                this.error = false;
                console.log("statistics", data)
                if ('diagnosis' in this.stats) {
                    this.diagnosis = this.stats.diagnosis;
                    this.count = this.stats.count;
                }
            }, (error) => {
                this.error = true;
                console.log("statistics", error)
            });
        }, (error) => {
            this.noresult = true;
            this.message = error.message;

        });


    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }




}

