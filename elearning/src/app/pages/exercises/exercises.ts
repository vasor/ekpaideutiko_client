import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { AuthService } from '../../auth.service';
@Component({
    selector: 'exercises',
    templateUrl: './exercises.html',
    styleUrls: ['./exercises.css']
})
export class ExercisesPage {
    private user_id: String;

    constructor(private auth: AuthService) {
        this.auth = auth;
        this.user_id = this.auth.user;
    }
}
