import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { User } from '../../services/user';

@Component({
	selector: 'home',
	templateUrl: './home.html',
	styleUrls: ['./home.css']
})
export class HomePage {
	public loginForm: FormGroup;
	private username: any;
	private password: any;
	private success: Boolean;
	private message: String;
	constructor(public userService: User, public formBuilder: FormBuilder, private auth: AuthService, private router: Router) {
		this.loginForm = formBuilder.group({
			username: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*')])],
			password: ['', Validators.compose([Validators.maxLength(30), Validators.required])]
		});
		this.success = true;
		if (this.auth.authenticated()) {
			this.router.navigateByUrl('/start')
		}


	}

	login() {
		let credentials = { username: this.username, password: this.password }
		this.auth.login(credentials).then(
			(success) => {
				console.log("SUCCESS");
				this.success = true;
				this.userService.setLocalInfo();
				this.router.navigateByUrl('/start');
			},
			(err) => {
				console.log(err);

				this.success = false;
				this.message = err.json()['message'];
			});
	}

	visitorLogin() {
		this.router.navigateByUrl('/start');
	}



}

