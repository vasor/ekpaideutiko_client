import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
@Component({
    selector: 'register',
    templateUrl: './register.html',
    styleUrls: ['./register.css']
})
export class RegisterPage {
    title = 'Register Page!';
    public slideOneForm: FormGroup;

    submitAttempt: boolean = false;
    public username: String;
    public email: String;
    public firstname: String;
    public lastname: String;
    public password: String;
    public repassword: String;
    public passwordGroup: any;
    public not_matching: Boolean;
    public loading: any;
    public result: any;
    public token: any;
    private success: Boolean;
    private message: String;
    private gender: String;
    private gender_tag: String;
    constructor(public formBuilder: FormBuilder, private userService: User, private router: Router, private auth: AuthService) {
        if (this.auth.authenticated()) {
            this.router.navigateByUrl('/start')
        }
        this.slideOneForm = formBuilder.group({
            gender: ['', Validators.required],
            username: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*')])],
            firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            email: ['', Validators.compose([Validators.required, Validators.pattern(/^[a-z0-9!#$%&' * +\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)])],
            password: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
            repassword: ['', Validators.compose([Validators.maxLength(30), Validators.required])]
        }, {
                validator: this.matchingPasswords('password', 'repassword')
                // matching_password: formBuilder.group({
                //   password: ['', Validators.required],
                //   confirm: ['', Validators.required]
                // }, this.matchPassword)
            });
        this.passwordGroup = this.slideOneForm.controls['matching_password'];
        this.success = true;
        this.result = [];
        this.gender = "0";
        this.gender_tag = "αγόρι";

    }

    matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];

            if (password.value !== confirmPassword.value) {

                this.not_matching = true;
                return {
                    mismatchedPasswords: true
                };
            }
        }
    }

    save() {

        this.submitAttempt = true;
        if (this.slideOneForm.valid) {

            console.log("success!", this.username, this.lastname, this.firstname, this.email, this.password)
            this.auth.signup(this.firstname, this.lastname, this.username, this.email, this.password, this.gender)
                .then((data) => {
                    console.log(data['token']);
                    this.token = data['token'];
                    this.saveJwt(this.token);
                    // setTimeout(() => {
                    //     this.router.navigateByUrl('/start');
                    // }, 2000)
                    this.router.navigateByUrl('/start');
                    // this.saveJwt(this.result.token)
                },
                (error) => {
                    console.log(error)

                    this.success = false;
                    this.message = error.json()['message'];

                })
        }
        else {
            console.log("not valid")
        }
    }




    saveJwt(jwt) {
        console.log(jwt)
        if (jwt) {
            localStorage.setItem('token', jwt)
        }
    }
}
