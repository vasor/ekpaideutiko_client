import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { Router, ActivatedRoute } from '@angular/router';
import { MathService } from '../../services/mathService';
@Component({
    selector: 'edit-content',
    templateUrl: './edit-content.html',
    styleUrls: ['./edit-content.css']
})
export class EditContentPage {
    private title: String;
    private description: String;
    private data: any;
    private category: String;
    private id: String;

    constructor(private mathService: MathService, private route: ActivatedRoute, private router: Router) {
        this.mathService = mathService;


    }

    ngOnInit() {
        this.route.queryParams.subscribe((params) => {
            this.description = params['description'];
            this.category = params['category'];
            this.title = params['title'];
            this.id = params['id'];
        });
    }

    editContent() {
        let item = { 'category': this.category, 'title': this.title, 'description': this.description, 'id': this.id };
        console.log(item, "before sent")
        this.mathService.editContent(item).subscribe(
            (data) => {
                console.log(data);
                switch (this.category) {
                    case 'πρόσθεση': {
                        this.router.navigateByUrl('/theory/addition');
                        break;
                    }
                    case 'αφαίρεση': {
                        this.router.navigateByUrl('/theory/substraction');
                        break;
                    }
                    case 'διαίρεση': {
                        this.router.navigateByUrl('/theory/division');
                        break;
                    }
                    case 'πολλαπλασιασμός': {
                        this.router.navigateByUrl('/theory/multiplication');
                        break;
                    }


                }
            }, (error) => {
                console.log(error);
            }, () => {

            }
        );
    }
}
