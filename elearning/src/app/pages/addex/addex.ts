import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { MathService } from '../../services/mathService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'addex',
    templateUrl: './addex.html',
    styleUrls: ['./addex.css']
})
export class AddExPage implements OnInit {
    private op1: number;
    private op2: number;
    private op3: number;
    private operator: String;
    private result: number;
    private category: String;
    private sub: any;
    public slideOneForm: FormGroup;
    constructor(public formBuilder: FormBuilder, private mathService: MathService, private route: ActivatedRoute, private router: Router) {
        this.mathService = mathService;

        this.slideOneForm = formBuilder.group({
            op1: ['', Validators.required],
            op2: ['', Validators.required],
            op3: [''],
            result: ['', Validators.required],
        });
        this.category = "";




    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            console.log(params);
            this.category = params['category']; // (+) converts string 'id' to a number
            console.log(this.category);
            // In a real app: dispatch action to load the details here.
        });

    }

    addExercise() {
        if (this.op3 == undefined) {
            this.op3 = 0;
        }

        switch (this.category) {
            case 'addition': {
                this.category = "πρόσθεση";
                this.operator = "+";

                break;
            }
            case 'substraction': {
                this.category = "αφαίρεση";
                this.operator = "-";
                this.op3 = 0;
                break;
            }
            case 'division': {
                this.category = "διαίρεση";
                this.operator = ":";
                this.op3 = 0;
                break;
            }
            case 'multiplication': {
                this.category = "πολλαπλασιασμός";
                this.operator = "x";
                this.op3 = 0;
                break;
            }

        }

        let item = { 'op1': this.op1, 'op2': this.op2, 'op3': this.op3, 'category': this.category, 'operator': this.operator, 'result': this.result }
        console.log(item)
        this.mathService.addExercise(item).subscribe((data) => {
            console.log(data);
            console.log(this.category);
            this.router.navigateByUrl('/exercises');
        }, (error) => {
            console.log(error);
        }, () => {

        });
    }

}
