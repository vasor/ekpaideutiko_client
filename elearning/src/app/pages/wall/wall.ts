import { Component, Pipe, PipeTransform, ElementRef, ViewChild, OnInit, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { MathService } from '../../services/mathService';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { ReversePipe } from '../../reverse';

@Component({
    selector: 'wall',
    templateUrl: './wall.html',
    styleUrls: ['./wall.css']
})
export class WallPage implements OnInit, AfterViewChecked {

    @ViewChild('scrollMe') private myScrollContainer: ElementRef;
    public data: any;
    public user: any;
    public post: String;
    constructor(public mathService: MathService, public auth: AuthService, public router: Router, public userService: User) {
        this.mathService = mathService;
        this.auth = auth;
        this.data = [];
        this.post = "";

        if (this.auth.user) {
            if (!this.userService.hasLocalInfo()) {
                this.userService.setLocalInfo()
            }
            this.user = this.userService.user;
            this.getData();
        }
        else {
            this.router.navigateByUrl('/start')
        }


    }



    ngAfterViewChecked() {
        this.scrollToBottom();
    }

    scrollToBottom(): void {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch (err) { }
    }

    ngOnInit() {

        if (this.auth.user) {
            if (!this.userService.hasLocalInfo()) {
                this.userService.setLocalInfo()
            }
            this.user = this.userService.user;
            this.getData();
        }
        else {
            this.router.navigateByUrl('/start')
        }
        this.scrollToBottom();
    }

    getData() {
        this.mathService.loadWallData().subscribe((data) => {
            this.data = data;
            // console.log(data);
        }, (error) => {
            console.log(error);
            this.data = { 'username': 'SYSTEM', 'post': 'ΚΑΤΙ ΠΗΓΕ ΣΤΡΑΒΑ. ΠΑΡΑΚΑΛΩ ΦΟΡΤΩΣΤΕ ΞΑΝΑ ΤΗ ΣΕΛΙΔΑ' }
        });
    }

    addPost() {
        console.log(this.post.trim().length)
        if (!(this.post.trim().length == 0)) {
            // console.log("not empty")
            // console.log(this.user)
            this.mathService.sendPost(this.post, this.user.username).subscribe((data) => {
                // console.log(data);
                this.data[this.data.length] = { 'username': this.user.username, 'post': this.post }
                this.post = "";
                this.scrollToBottom();
                // console.log(this.user)

            }, (error) => {
                console.log(error);
                alert('ΚΑΤΙ ΠΗΓΕ ΣΤΡΑΒΑ. ΠΑΡΑΚΑΛΩ ΦΟΡΤΩΣΤΕ ΞΑΝΑ ΤΗ ΣΕΛΙΔΑ')
            })
        }

    }


}
