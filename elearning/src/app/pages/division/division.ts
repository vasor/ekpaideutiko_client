import { Component, ViewChild, ElementRef, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { MathService } from '../../services/mathService';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { ConfirmComponent } from '../../confirm.component';
import { DialogService } from "ng2-bootstrap-modal";

@Component({
    selector: 'division',
    templateUrl: './division.html',
    styleUrls: ['./division.css']
})
export class DivisionPage {

    private titles: any;
    private descriptions: any;
    private count: Number;
    private noResults: Boolean;
    private data: any;
    private ratings: any;
    private voted: Boolean;
    private user_id: String;
    constructor(private dialogService:DialogService,private mathService: MathService, private userService: User, private auth: AuthService, private router: Router) {
        this.mathService = mathService;
        this.data = [];
        this.titles = [];
        this.descriptions = [];
        this.ratings = [];
        this.noResults = true;
        this.voted = false;
        this.auth = auth;
        this.user_id = this.auth.user;
    }

    ngOnInit() {
        this.mathService.getTheory("διαίρεση").subscribe((data) => {
            console.log(data);
            this.count = data.data.length;
            this.noResults = false;
            this.ratings = data.ratings;
            for (let i = 0; i < this.count; i++) {
                this.data[i] = { 'title': data.data[i].title, 'description': data.data[i].description, 'id': data.data[i]._id };
            }
            console.log(this.data, this.ratings);


        },
            (error) => {
                console.log(error);
                this.noResults = true;
            },
            () => {
                console.log("loaded")
            })
    }

    rateUp() {
        this.voted = true;
        this.ratings.up++;
        this.userService.rateUp("διαίρεση").subscribe((data) => {
            console.log(data);
        }, (error) => {
            console.log(error);
        }, () => {
            console.log("updated");
        })
    }

    rateDown() {
        this.voted = true;
        this.ratings.down++;
        this.userService.rateDown("διαίρεση").subscribe((data) => {
            console.log(data);
        }, (error) => {
            console.log(error);
        }, () => {
            console.log("updated");
        })
    }

    addContent() {

        this.router.navigate(['/content/add'], { queryParams: { category: 'διαίρεση' } });
    }
    editContent(item) {
        console.log(item);
        this.router.navigate(['/content/edit'], { queryParams: { category: 'διαίρεση', title: item.title, description: item.description, id: item.id } });
    }

    deleteContent(item) {
            let disposable = this.dialogService.addDialog(ConfirmComponent, {
                title:'ΕΠΙΒΕΒΑΙΩΣΗ',
                message:'Διαγραφή?'})
                .subscribe((isConfirmed)=>{
                    //We get dialog result
                    if(isConfirmed) {
                        // alert('accepted');
                        console.log(item)
                        this.mathService.deleteContent(item.id).subscribe((data) => {
                            console.log("success");
                            location.reload();
                        }, (error) => {
                            console.log("not successful");
                            this.router.navigateByUrl('/theory')
                        }), () => {
                            console.log("function completed")
                        };
                    }
                    else {
                        // alert('declined');
                    }
                });
            //We can close dialog calling disposable.unsubscribe();
            //If dialog was not closed manually close it by timeout
            setTimeout(()=>{
                disposable.unsubscribe();
            },10000);

    }

}
