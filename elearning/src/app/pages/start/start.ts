import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { AuthService } from '../../auth.service';
@Component({
    selector: 'start',
    templateUrl: './start.html',
    styleUrls: ['./start.css']
})
export class StartPage implements OnInit {
    private user_id: String;
    constructor(private userService: User, private auth: AuthService) {
        // this.user_id = this.auth.user;
        this.auth = auth;
        this.user_id = this.auth.user;

    }

    ngOnInit() {
        this.user_id = this.auth.user;
    }



}
