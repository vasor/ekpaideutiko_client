import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { MathService } from '../../services/mathService';

@Component({
    selector: 'search',
    templateUrl: './search.html',
    styleUrls: ['./search.css']
})
export class SearchPage {

    public searched: Boolean;
    public searchInput: String;
    public filter: any;
    public results: any;
    public message: Boolean;
    public filter_name: String;
    public messageString: String;
    public showDropDown: Boolean;
    constructor(private mathService: MathService) {
        this.searched = false;
        this.searchInput = "";
        this.mathService = mathService;
        this.results = [];
        this.message = false;
        this.messageString = "";
        this.filter = "0";
        this.showDropDown = false;
    }

    search() {
        // if (this.filter == "0") this.filter_name = "Όλα τα αποτελέσματα"
        // else if (this.filter == "1") this.filter_name = "Κορίτσι"
        // else if (this.filter == "2") this.filter_name = "Αγόρι"
        // && (this.searchInput.trim().length > 2)
        //  if (!(this.searchInput.trim().length == 0)) {
        this.showDropDown = false;
        this.searched = true;
        console.log(this.searchInput, this.filter)
        this.mathService.search(this.searchInput, this.filter).subscribe((data) => {
            console.log(data);
            this.results = data;
            if (this.results.length == 0) {
                this.message = true;
                this.messageString = "ΔΕΝ ΥΠΑΡΧΕΙ ΧΡΗΣΤΗΣ ΜΕ ΑΥΤΑ ΤΑ ΚΡΙΤΗΡΙΑ"
                this.searched = false;
            }
        }, (error) => {

        });
    }

    dropdown() {
        this.showDropDown = true;
    }


}
