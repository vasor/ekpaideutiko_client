import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { AuthService } from '../../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    selector: 'me',
    templateUrl: './me.html',
    styleUrls: ['./me.css']
})
export class MePage implements OnInit {
    public user: any;
    public stats: any;
    public error: Boolean;
    public diagnosis: any;
    public count: number;
    constructor(public userService: User, public auth: AuthService, public router: Router, public route: ActivatedRoute) {
        this.diagnosis = "";
        this.count = 0;
        this.user = ""
        this.stats = []
        console.log(this.auth.user)
        if (!this.auth.user) {
            this.router.navigateByUrl('/home')
        }
        else {
            if (this.userService.hasLocalInfo()) {
                this.user = this.userService.user;
            }
            else {
                this.userService.setLocalInfo()
                this.user = this.userService.user;
            }
        }
        this.userService.getStatistics(this.auth.user).then((data) => {
            this.stats = data;
            this.error = false;
            console.log("statistics", data)
            if ('diagnosis' in this.stats) {
                this.diagnosis = this.stats.diagnosis;
                this.count = this.stats.count;

            }
        }, (error) => {
            this.error = true;
            console.log("statistics", error)
        });
    }

    ngOnInit() {
        // console.log(this.auth.user)
        // if (!this.auth.user) {
        //     this.router.navigateByUrl('/home')
        // }
        // else {
        //     if (this.userService.hasLocalInfo()) {
        //         this.user = this.userService.user;
        //     }
        //     else {
        //         this.userService.setLocalInfo()
        //         this.user = this.userService.user;
        //     }
        // }
        // this.userService.getStatistics(this.auth.user).then((data) => {
        //     this.stats = data;
        //     this.error = false;
        //     console.log("statistics", data)
        // }, (error) => {
        //     this.error = true;
        //     console.log("statistics", error)
        // });
    }




}

