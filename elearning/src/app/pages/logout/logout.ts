import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { Router } from '@angular/router'


@Component({
    selector: 'logout',
    templateUrl: './logout.html',
    styleUrls: ['./logout.css']
})
export class LogoutPage {
    private isLoggedOut: Boolean;
    private message: String;
    private countdown: number;
    constructor(private userService: User, private router: Router) {
        this.message = "GOOD BYE";
        this.countdown = 3;
    }

    ngOnInit() {
        setTimeout(() => this.countdown--
            , 1000);
        setTimeout(() => this.countdown--
            , 2000);
        setTimeout(() => this.countdown--
            , 3000);
        setTimeout(() => {

            this.isLoggedOut = this.userService.logout();
            if (this.isLoggedOut) {
                // location.reload();
                this.router.navigateByUrl('/home');


            }
            else {
                this.message = "Something went wrong. Please refresh the page to complete the log out"
            }
        }, 4000);

    }



}
