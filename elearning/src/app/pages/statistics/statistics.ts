import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { User } from '../../services/user';
import { MathService } from '../../services/mathService';
@Component({
    selector: 'statistics',
    templateUrl: './statistics.html',
    styleUrls: ['./statistics.css']
})
export class StatisticsPage {
    private filter: any;
    private stats: any;
    private statsg: any;
    private statsb: any;
    private error: Boolean;
    constructor(private userService: User, private mathService: MathService) {
        this.mathService = mathService;
        this.userService = userService;

        this.filter = "0";
        this.stats = []
        this.statsg = []
        this.statsb = []

        this.userService.getAllStatistics().then((data) => {
            this.stats = data;
            this.error = false;
            console.log("statistics", data)
        }, (error) => {
            this.error = true;
            console.log("statistics", error)
        });

        this.userService.getBoysStatistics().then((data) => {
            this.statsb = data;
            this.error = false;
            console.log("statistics", data)
        }, (error) => {
            this.error = true;
            console.log("statistics", error)
        });

        this.userService.getGirlsStatistics().then((data) => {
            this.statsg = data;
            this.error = false;
            console.log("statistics", data)
        }, (error) => {
            this.error = true;
            console.log("statistics", error)
        });

    }


}
