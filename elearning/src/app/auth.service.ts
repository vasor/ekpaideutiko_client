import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { AppComponent } from '../app/app.component';
import 'rxjs/add/operator/map';
import { User } from './services/user'

@Injectable()
export class AuthService {
    LOGIN_URL: string = "http://localhost:5000/login";
    SIGNUP_URL: string = "http://localhost:5000/signup";
    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    jwtHelper: JwtHelper = new JwtHelper();
    user: string;
    error: string;
    username: string;
    email: string;
    name: string;
    surname: string;
    registered_at: string;
    user_info: any;
    constructor(private http: Http) {
        let token = localStorage.getItem('token');
        if (token) {
            this.user = this.jwtHelper.decodeToken(token)['sub'];
        }
    }
    public authenticated() {
        return tokenNotExpired();
    }
    login(credentials) {
        return new Promise((resolve, reject) => {
            this.http.post(this.LOGIN_URL, JSON.stringify(credentials), { headers: this.contentHeader })
                .map(res => res.json())
                .subscribe(
                data => {
                    this.authSuccess(data.token);
                    resolve(data)
                },
                err => {
                    this.error = err;
                    reject(err)
                }
                );
        });
    }
    signup(firstname, lastname, username, email, password, gender) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        if (gender == "0") {
            gender = "αγόρι";
        }
        else {
            gender = "κορίτσι";
        }
        let body = {
            firstname: firstname,
            lastname: lastname,
            username: username,
            email: email,
            password: password,
            gender: gender
        };
        return new Promise((resolve, reject) => {
            this.http.post('http://localhost:5000/signup', JSON.stringify(body), { headers: headers })
                .map(res => res.json())
                .subscribe(
                (data) => {
                    this.authSuccess(data.token);
                    resolve(data)
                },
                err => {
                    this.error = err;
                    reject(err)
                }
                );
        });
    }
    logout() {
        localStorage.removeItem('token');
        this.user = null;
    }
    authSuccess(token) {
        this.error = null;
        localStorage.setItem('token', token);
        this.user = this.jwtHelper.decodeToken(token)['sub'];
        console.log(this.user);
    }


    loggedIn() {
        return tokenNotExpired();
    }
}