import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomePage } from './pages/home/home';
import { StartPage } from './pages/start/start';
import { MePage } from './pages/me/me';
import { TestPage } from './pages/test/test';
import { ExercisesPage } from './pages/exercises/exercises';
import { ProfilePage } from './pages/profile/profile';
import { SearchPage } from './pages/search/search';
import { HelpPage } from './pages/help/help';
import { AddContentPage } from './pages/add-content/add-content';
import { EditContentPage } from './pages/edit-content/edit-content';
import { StatisticsPage } from './pages/statistics/statistics';
import { TheoryPage } from './pages/theory/theory';
import { AdditionPage } from './pages/addition/addition';
import { SubstractionPage } from './pages/substraction/substraction';
import { MultiplicationPage } from './pages/multiplication/multiplication';
import { DivisionPage } from './pages/division/division';
import { WallPage } from './pages/wall/wall';
import { AddExPage } from './pages/addex/addex';
import { LogoutPage } from './pages/logout/logout';
import { RegisterPage } from './pages/register/register';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { MathService } from './services/mathService'
import { AuthService } from './auth.service';
import { Http, RequestOptions } from '@angular/http';
import { User } from './services/user';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { ConfirmComponent } from './confirm.component';
import { ReversePipe } from './reverse';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    tokenName: 'token',
    tokenGetter: (() => sessionStorage.getItem('token')),
    globalHeaders: [{ 'Content-Type': 'application/json' }],
  }), http, options);
}


const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomePage },
  { path: 'me', component: MePage },
  { path: 'register', component: RegisterPage },
  { path: 'exercises/add/:category', component: AddExPage },
  { path: 'start', component: StartPage },
  { path: 'exercises', component: ExercisesPage },
  { path: 'profile/:username', component: ProfilePage },
  { path: 'search', component: SearchPage },
  { path: 'statistics', component: StatisticsPage },
  { path: 'theory', component: TheoryPage },
  { path: 'wall', component: WallPage },
  { path: 'help', component: HelpPage },
  { path: 'logout', component: LogoutPage },
  { path: 'test/:category', component: TestPage },
  { path: 'content/edit', component: EditContentPage },
  { path: 'content/add', component: AddContentPage },
  { path: 'theory/addition', component: AdditionPage },
  { path: 'theory/substraction', component: SubstractionPage },
  { path: 'theory/multiplication', component: MultiplicationPage },
  { path: 'theory/division', component: DivisionPage }
];

@NgModule({
  declarations: [
    AppComponent,
    HomePage,
    RegisterPage,
    StartPage,
    ExercisesPage,
    ProfilePage,
    SearchPage,
    StatisticsPage,
    TheoryPage,
    WallPage,
    HelpPage,
    AdditionPage,
    SubstractionPage,
    MultiplicationPage,
    DivisionPage,
    LogoutPage,
    EditContentPage,
    AddContentPage,
    ConfirmComponent,
    ReversePipe,
    TestPage,
    MePage,
    AddExPage
  ],
  imports: [RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BootstrapModalModule
  ],
  providers: [AuthService, MathService, User, {
    provide: AuthHttp,
    useFactory: authHttpServiceFactory,
    deps: [Http, RequestOptions]
  }],
  entryComponents: [
    ConfirmComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
