import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { AuthService } from '../auth.service'

/*
  Generated class for the User provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class User {
    // @ViewChild(NavController) nav;
    public nav: any;
    public token: any;
    public loading: any;
    public result: any;
    public user: any;

    constructor(public http: Http, public auth: AuthService) {
        console.log('Hello User Provider');
        this.user = {}
        this.setLocalInfo();
        this.user = {
            username: localStorage.getItem('username'),
            name: localStorage.getItem('name'),
            surname: localStorage.getItem('surname'),
            email: localStorage.getItem('email'),
            registered_at: localStorage.getItem('registered_at')
        }

    }

    ngOnInit() {
        this.setLocalInfo();
        this.user = {
            username: localStorage.getItem('username'),
            name: localStorage.getItem('name'),
            surname: localStorage.getItem('surname'),
            email: localStorage.getItem('email'),
            registered_at: localStorage.getItem('registered_at')
        }
    }


    // signup(firstname, lastname, username, email, password, gender) {
    //     let headers = new Headers();
    //     headers.append('Content-Type', 'application/json');
    //     if (gender == "0") {
    //         gender = "αγόρι";
    //     }
    //     else {
    //         gender = "κορίτσι";
    //     }
    //     let body = {
    //         firstname: firstname,
    //         lastname: lastname,
    //         username: username,
    //         email: email,
    //         password: password,
    //         gender: gender
    //     };




    //     return this.http.post('http://localhost:5000/signup', JSON.stringify(body), { headers: headers })
    //         .map(res => res.json())
    // }



    getInfo(user_id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            user_id: user_id
        };



        return this.http.post('http://localhost:5000/info', JSON.stringify(body), { headers: headers })
            .map(res => res.json());
    }

    rateUp(category) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            category: category
        };



        return this.http.post('http://localhost:5000/rateUp', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    rateDown(category) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            category: category
        };



        return this.http.post('http://localhost:5000/rateDown', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
        localStorage.removeItem('name');
        localStorage.removeItem('surname');
        localStorage.removeItem('email');
        localStorage.removeItem('registered_at');
        this.auth.user = null;
        if (localStorage.getItem('token')) {
            return false;
        }
        else {
            return true;
        }
    }

    hasLocalInfo() {
        if (localStorage.getItem('username') && localStorage.getItem('name') && localStorage.getItem('surname') && localStorage.getItem('email') && localStorage.getItem('registered_at')) {
            return true;
        }
        else return false;
    }

    setLocalInfo() {
        this.getInfo(this.auth.user).subscribe((data) => {
            localStorage.setItem('username', data.username);
            localStorage.setItem('name', data.name);
            localStorage.setItem('surname', data.surname);
            localStorage.setItem('email', data.email);
            localStorage.setItem('registered_at', data.registered_at);
            this.user = data;
        }, (error) => {
            console.log("something went wrong")
        })
    }

    getStatistics(user_id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let body = {
            user_id: user_id
        };
        return new Promise((resolve, reject) => {
            this.http.post('http://localhost:5000/statistics', JSON.stringify(body), { headers: headers })
                .map(res => res.json())
                .subscribe(
                (data) => {

                    resolve(data)
                },
                err => {

                    reject(err)
                }
                );
        });
    }

    getAllStatistics() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return new Promise((resolve, reject) => {
            this.http.get('http://localhost:5000/all_statistics', { headers: headers })
                .map(res => res.json())
                .subscribe(
                (data) => {

                    resolve(data)
                },
                err => {

                    reject(err)
                }
                );
        });
    }

    getBoysStatistics() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return new Promise((resolve, reject) => {
            this.http.get('http://localhost:5000/boys_statistics', { headers: headers })
                .map(res => res.json())
                .subscribe(
                (data) => {

                    resolve(data)
                },
                err => {

                    reject(err)
                }
                );
        });
    }

    getGirlsStatistics() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return new Promise((resolve, reject) => {
            this.http.get('http://localhost:5000/girls_statistics', { headers: headers })
                .map(res => res.json())
                .subscribe(
                (data) => {

                    resolve(data)
                },
                err => {

                    reject(err)
                }
                );
        });
    }



}
