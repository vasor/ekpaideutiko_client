import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';


@Injectable()
export class MathService {

    constructor(public http: Http) {
    }


    getTheory(category) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            category: category
        };

        return this.http.post('http://localhost:5000/getTheory', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    deleteContent(id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            id: id
        };

        return this.http.post('http://localhost:5000/deleteContent', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    editContent(item) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            id: item.id,
            title: item.title,
            description: item.description,
            category: item.category
        };

        return this.http.post('http://localhost:5000/editContent', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    addContent(item) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            title: item.title,
            description: item.description,
            category: item.category
        };

        return this.http.post('http://localhost:5000/addContent', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    loadWallData() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.get('http://localhost:5000/loadWallData', { headers: headers })
            .map(res => res.json())
    }

    sendPost(post, username) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            username: username,
            post: post
        };

        return this.http.post('http://localhost:5000/addPost', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    search(searchInput, filter) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            search: searchInput,
            filter: filter
        };

        return this.http.post('http://localhost:5000/search', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    getProfileInfo(username) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            username: username
        };

        return this.http.post('http://localhost:5000/getProfileInfo', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    loadTests(category, user_id) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            category: category,
            user_id: user_id
        };

        return new Promise((resolve, reject) => {
            this.http.post('http://localhost:5000/loadTests', JSON.stringify(body), { headers: headers })
                .map(res => res.json())
                .subscribe(
                (data) => {
                    console.log(data)
                    resolve(data)
                },
                err => {
                    console.log(err)
                    reject(err)
                }
                );
        });
    }

    loadGeneralTests() {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return new Promise((resolve, reject) => {
            this.http.get('http://localhost:5000/generalTests', { headers: headers })
                .map(res => res.json())
                .subscribe(
                (data) => {

                    resolve(data)
                },
                err => {

                    reject(err)
                }
                );
        });
    }

    saveAnswer(answer, category, user_id, result, diagnosis) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            exercise_id: answer,
            user_id: user_id,
            success: result,
            category: category,
            diagnosis: diagnosis
        };

        return this.http.post('http://localhost:5000/saveAnswer', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    deleteExercise(id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            id: id
        };

        return this.http.post('http://localhost:5000/deleteExercise', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }

    addExercise(item) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            op1: item.op1,
            op2: item.op2,
            op3: item.op3,
            operator: item.operator,
            result: item.result,
            category: item.category
        };

        return this.http.post('http://localhost:5000/addExercise', JSON.stringify(body), { headers: headers })
            .map(res => res.json())
    }



}
