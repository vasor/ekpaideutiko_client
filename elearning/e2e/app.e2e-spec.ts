import { ElearningPage } from './app.po';

describe('elearning App', function() {
  let page: ElearningPage;

  beforeEach(() => {
    page = new ElearningPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
